<html lang="es"><head>
        <meta charset="utf-8">
        <title>Start</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" href="css/bootstrap.css" media="screen">
        <link rel="stylesheet" href="css/startpage.css">
    </head>

    <body class="bg-dark" style="transition: background-color 0.2s linear; -moz-transition: none;">

        <!--Nav-->
    <div class="navbar navbar-expand-lg fixed-top navbar-dark bg-primary">
        <div class="container">
            <a href="../" class="navbar-brand">StartPage</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav">

                </ul>

                <ul class="nav navbar-nav ml-auto">
                    
                    <li class="nav-item">
                        <a class="nav-link" href="http://gmail.com">Gmail</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="apps" aria-expanded="false">Google <span class="caret"></span></a>
                        <div class="dropdown-menu" aria-labelledby="apps" style="margin-left: -40px;">
                            <a class="dropdown-item" href="https://www.google.es/webhp?tab=ww">
                                <img src="img/search.png" alt="">
                            </a>
                            <a class="dropdown-item" href="https://www.youtube.com/">
                                <img src="img/yt.png" alt="">
                            </a>

                            <a class="dropdown-item" href="https://drive.google.com/drive/my-drive">
                                <img src="img/drive.png" alt="">
                            </a>
                            <a class="dropdown-item" href="https://www.google.es/maps?hl=es&amp;tab=wl">
                                <img src="img/maps.png" alt="">
                            </a>

                            <a class="dropdown-item" href="https://calendar.google.com/calendar/r">
                                <img src="img/cal.png" alt="">
                            </a>
                            <a class="dropdown-item" href="https://keep.google.com/u/0/">
                                <img src="img/keep.png" alt="">
                            </a>

                            <a class="dropdown-item" href="https://photos.google.com/?tab=wq&amp;pageId=none">
                                <img src="img/fotos.png" alt="">
                            </a>
                            <a class="dropdown-item" href="https://translate.google.es/?hl=es&amp;tab=wT">
                                <img src="img/trans.png" alt="">
                            </a>
                        </div>
                    </li>
                    

                </ul>
            </div>
        </div>
    </div>
    <!--/Nav-->



        <div class="container">

            <!--Content-->
            
    <div class="row justify-content-center d-flex align-items-center p-2" style="height: 450px;">

        <div class="col-8">
            <div class="card text-white bg-primary mb-3">
                <div class="card-header">Search Engines</div>
                    <div class="card-body">

                            <form method="get" class="form-inline my-2 my-lg-0 search-form ggl" action="https://www.google.com/search" name="search" style="display: initial;">
                                <input id="ggl-searchbox" style="width:100%" class="col-md-9 search-input form-control mr-sm-2" placeholder="Search" type="search" name="q">
                                <button class="col-md-2 search-button btn btn-secondary ml-auto my-2 my-sm-0" type="submit">Buscar</button>
                                <input type="hidden" name="ie" value="UTF-8">
                                <input type="hidden" name="channel" value="fe">
                                <input type="hidden" name="sa" value="Search">
                                <input title="search" type="hidden" name="hl" value="en">
                                <input type="hidden" name="client" value="browser-ubuntu">
                            </form>
                            <form class="form-inline my-2 my-lg-0 ddg" action="https://duckduckgo.com/" style="display:none">
                                <input id="ddg-searchbox" type="search" style="width:100%" class="col-md-9 search-input form-control mr-sm-2" name="q" autofocus="" placeholder="Search">
                                <button class="col-md-2 search-button btn btn-secondary ml-auto my-2 my-sm-0" type="submit">Buscar</button>
                            </form>
                            <form class="form-inline my-2 my-lg-0 yho" action="https://es.search.yahoo.com/search" style="display:none">
                                <input id="yho-searchbox" type="search" style="width:100%" class="col-md-9 search-input form-control mr-sm-2" name="q" autofocus="" placeholder="Search">
                                <button class="col-md-2 search-button btn btn-secondary ml-auto my-2 my-sm-0" type="submit">Buscar</button>
                            </form>

                            <fieldset class="form-group mt-3 mb-1 checksearch">
                                <div class="engines form-check form-check-inline">
                                    <label class="form-check-label mr-3">
                                        <input class="form-check-input" name="engine" id="ggl" type="radio" onclick="setSearch('ggl')">
                                        Google
                                    </label>

                                    <label class="form-check-label mr-3">
                                        <input class="form-check-input" name="engine" id="ddg" type="radio" onclick="setSearch('ddg')">
                                        DuckDuckGo
                                    </label>

                                    <label class="form-check-label mr-3">
                                        <input class="form-check-input" name="engine" id="yho" type="radio" onclick="setSearch('yho')">
                                        Yahoo
                                    </label>
                                </div>
                            </fieldset>

                    </div>
                </div>
            </div>
        </div>

    </div>



            <!--/Content-->

        

        <footer id="footer">

</footer>

        <script src="js/jquery.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/custom.js"></script>
        <script src="js/search.js"></script>

    

</body></html>
