

        if(localStorage.getItem("search") === null)
        {
            localStorage.setItem("search","ggl")
        }

        function hide(div)
        {
            document.getElementsByClassName(div)[0].style.display = 'none'
        }

        function show(div)
        {
            document.getElementsByClassName(div)[0].style.display = 'initial'
        }

        function setText(div,text)
        {
            document.getElementById(div).innerHTML = text
        }

        function appendText(div,text)
        {
            document.getElementById(div).append(text)
        }

        function setSearch(engine)
        {
            localStorage.setItem("search",engine);getSearch()
        }

        function getSearch()
        {
            engine = localStorage.getItem("search");hide("ggl");hide("ddg");hide("yho");
            if(engine == "ddg"){
                show("ddg");
                document.getElementById("ddg").checked = true;
                document.getElementById("ddg-searchbox").focus()
            }
            else if(engine == "yho"){
                show("yho");
                document.getElementById("yho").checked = true;
                document.getElementById("yho-searchbox").focus()
            }
            else{
                show("ggl");
                document.getElementById("ggl").checked = true;
                document.getElementById("ggl-searchbox").focus()
                }
        }

        setTimeout
        (
            function()
            {
                document.body.setAttribute("style","transition: background-color 0.2s linear; -moz-transition: none;")
            },500
        );
        if(localStorage.getItem("darkmode") === "true")
        {
            setDarkMode(true)
        }
        getSearch();
        setLocale();
